CREATE TABLE `dberp_other_warehouse_order` (
  `other_warehouse_order_id` int(11) NOT NULL AUTO_INCREMENT,
  `warehouse_id` int(11) NOT NULL,
  `warehouse_order_sn` varchar(50) NOT NULL,
  `warehouse_order_state` tinyint(1) NOT NULL DEFAULT '3',
  `warehouse_order_info` varchar(255) DEFAULT NULL,
  `warehouse_order_goods_amount` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `warehouse_order_tax` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `warehouse_order_amount` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `other_add_time` int(10) NOT NULL,
  `admin_id` int(11) NOT NULL,
  PRIMARY KEY (`other_warehouse_order_id`),
  KEY `other_warehouse_order_index` (`warehouse_id`,`warehouse_order_sn`,`warehouse_order_state`,`admin_id`),
  KEY `other_add_time` (`other_add_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='其他入库表';

CREATE TABLE `dberp_other_warehouse_order_goods` (
  `warehouse_order_goods_id` int(11) NOT NULL AUTO_INCREMENT,
  `other_warehouse_order_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `warehouse_goods_buy_num` int(11) NOT NULL,
  `warehouse_goods_price` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `warehouse_goods_tax` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `warehouse_goods_amount` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `goods_id` int(11) NOT NULL,
  `goods_name` varchar(100) NOT NULL,
  `goods_number` varchar(30) NOT NULL,
  `goods_spec` varchar(100) DEFAULT NULL,
  `goods_unit` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`warehouse_order_goods_id`),
  KEY `other_warehouse_order_goods_index` (`other_warehouse_order_id`,`warehouse_id`,`goods_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='其他入库商品表';

DROP TABLE IF EXISTS `dberp_stock_check`;
CREATE TABLE IF NOT EXISTS `dberp_stock_check` (
  `stock_check_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `stock_check_sn` varchar(50) NOT NULL COMMENT '盘点单号',
  `warehouse_id` int(11) NOT NULL COMMENT '仓库id',
  `stock_check_amount` decimal(19,4) NOT NULL DEFAULT '0.0000' COMMENT '盘点金额',
  `stock_check_user` varchar(100) NOT NULL COMMENT '盘点人',
  `stock_check_info` varchar(255) NOT NULL COMMENT '盘点备注',
  `stock_check_time` int(10) NOT NULL COMMENT '盘点时间',
  `stock_check_state` tinyint(1) NOT NULL DEFAULT '2' COMMENT '盘点状态，1 已盘点，2 待盘点',
  `admin_id` int(11) NOT NULL COMMENT '管理员id',
  PRIMARY KEY (`stock_check_id`),
  KEY `warehouse_id` (`warehouse_id`,`stock_check_amount`,`stock_check_state`,`admin_id`,`stock_check_sn`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='库存盘点表';

DROP TABLE IF EXISTS `dberp_stock_check_goods`;
CREATE TABLE IF NOT EXISTS `dberp_stock_check_goods` (
  `stock_check_goods_id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_check_id` int(11) NOT NULL,
  `stock_check_pre_goods_num` int(11) NOT NULL,
  `stock_check_aft_goods_num` int(11) NOT NULL,
  `stock_check_goods_amount` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `goods_id` int(11) NOT NULL,
  `goods_name` varchar(100) NOT NULL,
  `goods_number` varchar(30) NOT NULL,
  `goods_spec` varchar(100) NOT NULL,
  `goods_unit` varchar(20) NOT NULL,
  PRIMARY KEY (`stock_check_goods_id`),
  KEY `stock_check_id` (`stock_check_id`,`stock_check_goods_amount`,`goods_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='盘点商品表';

DROP TABLE IF EXISTS `dberp_stock_transfer`;
CREATE TABLE IF NOT EXISTS `dberp_stock_transfer` (
  `transfer_id` int(11) NOT NULL AUTO_INCREMENT,
  `transfer_sn` varchar(50) NOT NULL COMMENT '调拨单号',
  `transfer_in_warehouse_id` int(11) NOT NULL COMMENT '入库id',
  `transfer_out_warehouse_id` int(11) NOT NULL COMMENT '出库id',
  `transfer_add_time` int(10) NOT NULL COMMENT '添加时间',
  `transfer_finish_time` int(10) DEFAULT NULL COMMENT '完成时间',
  `transfer_info` varchar(500) DEFAULT NULL COMMENT '调拨备注',
  `transfer_state` tinyint(1) NOT NULL DEFAULT '0' COMMENT '调拨状态，0 待调拨，1 已调拨',
  `admin_id` int(11) NOT NULL COMMENT '管理员id',
  PRIMARY KEY (`transfer_id`),
  KEY `transfer_sn` (`transfer_sn`,`transfer_in_warehouse_id`,`transfer_out_warehouse_id`,`admin_id`,`transfer_state`),
  KEY `transfer_add_time` (`transfer_add_time`),
  KEY `transfer_finish_time` (`transfer_finish_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='库间调拨基本表';

DROP TABLE IF EXISTS `dberp_stock_transfer_goods`;
CREATE TABLE IF NOT EXISTS `dberp_stock_transfer_goods` (
  `transfer_goods_id` int(11) NOT NULL AUTO_INCREMENT,
  `transfer_id` int(11) NOT NULL,
  `in_warehouse_id` int(11) NOT NULL,
  `out_warehouse_id` int(11) NOT NULL,
  `transfer_goods_num` int(11) NOT NULL,
  `transfer_goods_state` tinyint(1) NOT NULL DEFAULT '0',
  `goods_id` int(11) NOT NULL,
  `goods_name` varchar(100) NOT NULL,
  `goods_number` varchar(30) NOT NULL,
  `goods_spec` varchar(100) DEFAULT NULL,
  `goods_unit` varchar(20) NOT NULL,
  PRIMARY KEY (`transfer_goods_id`),
  KEY `transfer_id` (`transfer_id`,`in_warehouse_id`,`out_warehouse_id`,`goods_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='库间调拨商品表';